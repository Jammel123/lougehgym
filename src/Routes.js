import Vue from "vue";
import Router from "vue-router";
import Layout from "@/components/Layout/Layout";
import Login from "@/pages/Login/Login";
import ErrorPage from "@/pages/Error/Error";
// Main
import AnalyticsPage from "@/pages/Dashboard/Dashboard";
//Member
import MemberPage from "@/pages/Member/Member";
//Membership
import MembershipPage from "@/pages/Membership/Membership";
//Services
import ServicesPage from "@/pages/Services/Services";
//Merchandise
import MerchandisePage from "@/pages/Merchandise/Merchandise";
//Walkin
import WalkinPage from "@/pages/Walkin/Walkin";
//LandingPage
import LandingPage from "@/pages/LandingPage/LandingPage";
//PlanPage
import PlanPage from "@/pages/LandingPage/Plan/Plan";
//ShopPage
import ShopPage from "@/pages/LandingPage/Shop/Shop";
//Order
import OrderPage from "@/pages/Order/Order";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/login",
      name: "Login",
      component: Login
    },

    {
      path: "/error",
      name: "Error",
      component: ErrorPage
    },

    {
      path: "/app",
      name: "Layout",
      component: Layout,
      children: [
        {
          path: "dashboard",
          name: "AnalyticsPage",
          component: AnalyticsPage
        },
        {
          path: "Member",
          name: "MemberPage",
          component: MemberPage
        },
        {
          path: "Membership",
          name: "MembershipPage",
          component: MembershipPage
        },

        {
          path: "Services",
          name: "ServicesPage",
          component: ServicesPage
        },

        {
          path: "Merchandise",
          name: "MerchandisePage",
          component: MerchandisePage
        },

        {
          path: "Walk-in",
          name: "WalkinPage",
          component: WalkinPage
        },

        {
          path: "Order",
          name: "OrderPage",
          component: OrderPage
        }
      ]
    },
    {
      path: "/landing",
      name: "landingPage",
      component: LandingPage,
      children: [
        {
          path: "/Shop",
          name: "ShopPage",
          component: ShopPage
        },
        {
          path: "/MembershipPlan",
          name: "PlanPage",
          component: PlanPage
        }
      ]
    }
  ]
});
